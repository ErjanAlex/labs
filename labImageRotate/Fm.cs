﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageRotate
{
    public partial class Fm : Form
    {
        private readonly Bitmap japan;

        public Fm()
        {
            InitializeComponent();
            japan = new Bitmap(Properties.Resources.Japan);

            trackBar1.ValueChanged += (s, e) => pictureBox1.Invalidate();
            pictureBox1.Paint += PictureBox1_Paint;

            button1.Click += (s, e) => {
                japan.RotateFlip(RotateFlipType.RotateNoneFlipX);
                pictureBox1.Invalidate();
            };


            button2.Click += (s, e) => {
                japan.RotateFlip(RotateFlipType.RotateNoneFlipY);
                pictureBox1.Invalidate();
            };

            

        }


        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.TranslateTransform(japan.Width / 2, japan.Height / 2);
            e.Graphics.RotateTransform(trackBar1.Value);
            e.Graphics.DrawImage(japan, -japan.Width / 2, -japan.Height / 2);
        }

        private void Fm_Load(object sender, EventArgs e)
        {

        }
    }
}
