﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/* ХВ :
 * 1. Добавить механизм загрузки других изображений (из ресурсов, из файлов);
 * 2. добавить механизм загрузки изображения в компоненты (левый-верхний угол, масштабировать. и т.п.);
 * 3. с клавишей Shift выделять/снимать выделение компонентов и визуально показывать какие компоненты выделены;
 * 4. совместное перемещение выделенных компонентов с помощью мышки;
 * 5. если компонент встал на свое место то он перестает перемещаться и становится ч/б;
 */

namespace labControlPuzzle
{
    public partial class Fm : Form
    {
        private const int HOLD_PIXEL = 25;

        enum SelMode
        {
            Select,
            Normal
        }
        private SelMode selMode;

        private PictureBox[,] pics;
        private int cellWidth;
        private int cellHeight;
        private Point startMouseDown;

        public int Rows { get; private set; } = 5;
        public int Cols { get; private set; } = 3;

        public Fm()
        {
            InitializeComponent();

            CreateCell();
            ResizeCell();
            StartLocationCells();
            //RandomLocationCell();

            this.KeyDown += Fm_KeyDown;
            this.KeyUp += Fm_KeyUp;

            this.Text += " : (F1 - Собрать, F2 - перемещать, F3 - другой размер)";
        }

        private void Fm_KeyUp(object sender, KeyEventArgs e)
        {
            selMode = SelMode.Normal;
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            selMode = e.Shift ? SelMode.Select : SelMode.Normal;

            switch (e.KeyCode)
            {
                case Keys.F1:
                    StartLocationCells();
                    break;

                case Keys.F2:
                    RandomLocationCell();
                    break;

                case Keys.F3:
                    ResizeCell();
                    break;
            }
        }

        private void RandomLocationCell()
        {
            Random rnd = new Random();
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j].Location = new Point(rnd.Next(this.ClientSize.Width - pics[i, j].Width),
                                                    rnd.Next(this.ClientSize.Height - pics[i, j].Height));
                }
        }

        private void StartLocationCells()
        {
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j].Location = new Point(j * cellWidth, i * cellHeight);
                }
        }

        private void ResizeCell()
        {
            cellWidth = this.ClientSize.Width / Cols;
            cellHeight = this.ClientSize.Height / Rows;

            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j].Width = cellWidth;
                    pics[i, j].Height = cellHeight;
                    pics[i, j].Tag = (i, j);
                    pics[i, j].BackgroundImage = new Bitmap(cellWidth, cellHeight);
                    var g = Graphics.FromImage(pics[i, j].BackgroundImage);
                    g.DrawImage(Properties.Resources.Castle,
                                new Rectangle(0, 0, cellWidth, cellHeight),
                                new Rectangle(j * cellWidth, i * cellHeight, cellWidth, cellHeight),
                                GraphicsUnit.Pixel);
                    g.DrawString($"[{i},{j}]",
                                 new Font("", 10, FontStyle.Bold),
                                 new SolidBrush(Color.Black),
                                 new Point(0, 0));
                    g.Dispose();
                }
        }

        private void CreateCell()
        {
            pics = new PictureBox[Rows, Cols];
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j] = new PictureBox();
                    pics[i, j].BorderStyle = BorderStyle.FixedSingle;

                    pics[i, j].MouseDown += PictureBoxAll_MouseDown;
                    pics[i, j].MouseMove += PictureBoxAll_MouseMove;
                    pics[i, j].MouseUp += PictureBoxAll_MouseUp;

                    this.Controls.Add(pics[i, j]);
                }
        }

        private void PictureBoxAll_MouseUp(object sender, MouseEventArgs e)
        {
            if (sender is Control x)
            {
                if (e.Button == MouseButtons.Left)
                {
                    var xLocation = x.Location;

                    for (int i = 0; i < Rows; i++)
                        for (int j = 0; j < Cols; j++)
                        {
                            if ((x.Location.X > j * cellWidth - HOLD_PIXEL) && (x.Location.X < j * cellWidth + 15))
                                xLocation.X = j * cellWidth;

                            if ((x.Location.Y > i * cellHeight - HOLD_PIXEL) && (x.Location.Y < i * cellHeight + 15))
                                xLocation.Y = i * cellHeight;
                        }
                    x.Location = xLocation;
                }
                x.Cursor = Cursors.Default;
            }
        }

        private void PictureBoxAll_MouseMove(object sender, MouseEventArgs e)
        {
            if (sender is Control x)
            {
                if (e.Button == MouseButtons.Left)
                {
                    x.Location = new Point(x.Location.X + e.X - startMouseDown.X,
                                           x.Location.Y + e.Y - startMouseDown.Y);
                }

                //(int r, int c) = ((int, int))x.Tag;
            }
        }

        private void PictureBoxAll_MouseDown(object sender, MouseEventArgs e)
        {
            startMouseDown = e.Location;
            if (sender is Control x)
            {
                x.BringToFront();
                x.Cursor = Cursors.SizeAll;
            }
        }
    }
}
