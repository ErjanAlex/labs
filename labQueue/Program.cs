﻿using System;
using System.Collections;

namespace labQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue x = new Queue();
            x.Enqueue(123);
            x.Enqueue("hello");
            x.Enqueue('y');
            x.Enqueue(new Queue());

            Console.WriteLine(x.Peek()); // Посмотреть очередь
            Console.WriteLine("---");
            while (x.Count > 0)
            {
                Console.WriteLine(x.Dequeue()); // Извлечение из очереди
            }
        }
    }
}
