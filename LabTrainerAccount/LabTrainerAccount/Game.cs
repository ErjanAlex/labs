﻿using System;
using System.ComponentModel.Design;

namespace LabTrainerAccount
{
    internal class Game
    {
        public int CountCorrect { get; private set; }
        public int CountWrong { get; private set; }
        public string CodeText { get; private set; }
        public int Level { get; private set; }

        private bool answerCorrect;
        public event EventHandler Change;

        internal void DoReset()
        {
            CountCorrect = 0;
            CountWrong = 0;
            Level = 1;
            DoContinue();
        }

        public void DoContinue()
        {
            Random rnd = new Random();
            int xValue1 = rnd.Next(10*Level);
            int xValue2 = rnd.Next(10*Level);

            int xResult;
            char znak;
            switch (rnd.Next(4))
            {
                case 1:
                    xResult = xValue1 * xValue2;
                    znak = '*';
                    break;
                case 2:
                    xResult = xValue1 + xValue2;
                    znak = '+';
                    break;
                case 3:
                    xResult = xValue1 - xValue2;
                    znak = '-';
                    break;
                default:
                    if (xValue2 == 0) xValue2++; 
                    xResult = xValue1 / xValue2;
                    znak = '/';
                    break;
            }

            int xResultNew = xResult;

            if (rnd.Next(2) == 1)
                xResultNew += rnd.Next(1, 5*Level) * (rnd.Next(2) == 1 ? 1 : -1);

            answerCorrect = xResult  == xResultNew;
            CodeText = $"{xValue1} {znak} {xValue2} = {xResultNew}";
            Change?.Invoke(this, EventArgs.Empty);
        }

        public void DoAnswer(bool v)
        {
            if (v == answerCorrect)
            {
                CountCorrect++;
            if (CountCorrect % 5 == 0) Level++;
            }
            else
                CountWrong++;
            
            DoContinue();
        }
    }
}