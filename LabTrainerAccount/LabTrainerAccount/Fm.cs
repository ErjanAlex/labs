﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabTrainerAccount
{
    public partial class Fm : Form
    {
        private readonly Game g;

        public Fm()
        {
            InitializeComponent();
            g = new Game();
            g.Change += G_Change;
            g.DoReset();

            buYes.Click += (s, e) => g.DoAnswer(true);
            buNo.Click += (s, e) => g.DoAnswer(false);
            buttonReset.Click += (s, e) => g.DoReset();
            buttonNext.Click += (s, e) => g.DoContinue();

            //Выражения на +-*/
            //Сброс статистики
            // Уровни сложности до 20, 40, 80 от колличества правильных ответов подряд
            //Уровеннь сложности на форме
        }

        private void G_Change(object sender, EventArgs e)
        {
            laCorrect.Text = $"Верно = {g.CountCorrect}";
            laWrong.Text = $"Не верно = {g.CountWrong}";
            laCode.Text = g.CodeText;
            labelLevel.Text = $"Уровень сложности : { g.Level}";
        }

        private void laCorrect_Click(object sender, EventArgs e)
        {

        }
    }
}
