﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageScrollVert
{
    public partial class Fm : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private readonly Bitmap t;
        private readonly Graphics h;
        private Bitmap imBG;
        private Bitmap imBG1;
        private Point startPoint;
        private int deltaX = 0;
        private int deltaX1 = 0;
        private int count = 0;
        Boolean tol = false;

        public Fm()


        {

            InitializeComponent();

           


            imBG = Properties.Resources.VertGame;
            imBG1 = Properties.Resources.carMMC;

            this.Height = imBG.Height;

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);

            g = Graphics.FromImage(b);

            h = Graphics.FromImage(b);

            this.DoubleBuffered = true;
            this.Paint += (s, e) => { UpdateBG(); e.Graphics.DrawImage(b, 0, 0); };
            this.KeyDown += Fm_KeyDown;
          



            /*    
             *  Вертикальная игра
             *  Бобин Илья
             */

            }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {


            switch (e.KeyCode)
            {

                case Keys.Up:
                     UpdateDeltaX(10);
                    break;

                case Keys.Down:
                     UpdateDeltaX(-10);
                    break;


                case Keys.Left:
                    UpdateDeltaX(10);
                    UpdateDeltaX1(-10);
                    break;

                case Keys.Right:
                    UpdateDeltaX(10);
                    UpdateDeltaX1(10);
                    break;
            }

            this.Invalidate();
        }

        

        private void UpdateDeltaX(int v)
        {
            Text = $"{Application.ProductName} : deltaX = {deltaX},deltaX1 = {deltaX1},v = {v}, count = {count}";
            deltaX += v;
          


            if (deltaX > 0)
            {
                deltaX -= imBG.Height;
            }
            else if (deltaX < -imBG.Height)
            {
                deltaX += imBG.Height;
            }


        }

        private void UpdateDeltaX1(int v)
        {

            if (deltaX1 <= 250 && deltaX1 >= -250) {
                deltaX1 += v;
                if (deltaX1 == 250)
                {
                    deltaX1 = 240;
                }
                else if (deltaX1 == -250)
                {
                    deltaX1 = -240;
                }
            }
                       
            }

        private void UpdateBG()
        {
            //g.Clear(SystemColors.Control);
            for (int i = 0; i < imBG.Height; i++)
            {
                g.DrawImage(imBG, 0, deltaX + i * imBG.Height);
            }

            h.DrawImage(imBG1, deltaX1 + (imBG.Width / 2)-imBG1.Width/2 , (imBG.Height / 2) + imBG1.Height / 2);

        }

     

    }
}
