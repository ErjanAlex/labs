﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabButtonsOnGrid
{
    public partial class Fm : Form
    {
        private Button[,] bu;
        public int Rows { get; private set; } = 3;
        public int Cols { get; private set; } = 4;
        public Fm()
        {
            InitializeComponent();
            CreateButtons();
            ResizeButtons();
            this.Resize += (s, e) => ResizeButtons();
            this.KeyPress += Fm_KeyPress;
        }

        private void Fm_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case (char)119://W
                    if (Rows < 9) 
                    {
                        Rows++;
                        CreateButtons();
                    }
                    break;
                case (char)115://S
                    if (Rows > 1)
                    {
                        Rows--;
                        CreateButtons();
                    }
                    break;
                case (char)97://S
                    if (Cols > 1)
                    {
                        Cols--;
                        CreateButtons();
                    }
                    break;
                case (char)100://S
                    if (Cols < 9)
                    {
                        Cols++;
                        CreateButtons();
                    }
                    break;
                default:
                    break;
            }
        }

        private void ResizeButtons()
        {
            int xCellWidth = this.ClientSize.Width / Cols;
            int xCellHeight = this.ClientSize.Height / Rows;

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    bu[i, j].Width = xCellWidth;
                    bu[i, j].Height = xCellHeight;
                    bu[i, j].Location = new Point(j * xCellWidth, i * xCellHeight);

                }
            }
        }

        private void CreateButtons()
        {
            this.Controls.Clear();
            bu = new Button[Rows, Cols];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    bu[i, j] = new Button();
                    bu[i, j].MouseEnter += Fm_MouseEnter;
                    bu[i, j].MouseLeave += Fm_MouseLeave;
                    bu[i, j].Text = $"[{i}, {j}]"; 
                    this.Controls.Add(bu[i, j]);
                }
            }
            ResizeButtons();
        }

        private void Fm_MouseLeave(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button.BackColor = default;
        }

        private void Fm_MouseEnter(object sender, EventArgs e)
        {
            Random rnd = new Random();
            Button button = (Button)sender;
            button.BackColor = Color.FromArgb(rnd.Next(255), rnd.Next(255), rnd.Next(255));
        }
    }
}
