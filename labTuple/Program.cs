﻿using System;

namespace labTuple
{
    class Program
    {
        static void Main(string[] args)
        {
            // (1)
            var x1 = (0, 1); // (int, int) x1 = (2, 4);
            Console.WriteLine(x1.Item1);
            Console.WriteLine(x1.Item2);
            Console.WriteLine();
            // (2) Название полей кортежа через объявления
            (int min, int max) x2 = (2, 3);
            Console.WriteLine(x2.min);
            Console.WriteLine(x2.max);
            Console.WriteLine();
            // (3) Название полей кортежа через инициализацию
            var x3 = (min: 4, max: 5);
            Console.WriteLine(x3.min);
            Console.WriteLine(x3.max);
            Console.WriteLine();
            // (4) Отдельные переменные для каждого поля кортежа
            var (min, max) = (6, 7);
            Console.WriteLine(min);
            Console.WriteLine(max);
            Console.WriteLine();
            // (5) Получение кортежа
            var x5 = GetX5();
            Console.WriteLine(x5.Item1);
            Console.WriteLine(x5.Item2);
            Console.WriteLine();
            // (6) Получение кортежа с именами
            var x6 = GetX6();
            Console.WriteLine(x6.min);
            Console.WriteLine(x6.max);
            Console.WriteLine();
            // (7) Передача кортежа в качестве параметра метода
            var x7 = GetX7((3, 10), 14);
            Console.WriteLine(x7.min);
            Console.WriteLine(x7.max);
            Console.WriteLine();
        }

        private static (int, int) GetX5() => (8, 9);
        private static (int min, int max) GetX6() => (10, 11);
        private static (int min, int max) GetX7((int, int) p, int v) => (p.Item1 + p.Item2, v);
    }
    }
