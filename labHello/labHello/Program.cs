﻿using System;

namespace LabHello
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Name?");
            string name = Console.ReadLine();

            Console.WriteLine("Surname?");
            string surname = Console.ReadLine();

            Console.WriteLine("Hello, {0} {1}", name, surname);

            Console.ReadLine();
        }
    }
}