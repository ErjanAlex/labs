﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabPaint
{
    public partial class Fm : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private Point startPoint;
        private Point startPointMove;
        private Pen myPen;


        // Сделано 
        // Бобин Илья

        // Добавить рисование фигур
        public Fm()
        {
            InitializeComponent();

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            myPen = new Pen(Color.Red, 10);
            myPen.StartCap = myPen.EndCap = System.Drawing.Drawing2D.LineCap.Round;

            pxImage.MouseDown += (s, e) => startPoint = e.Location;
            pxImage.MouseMove += PxImage_MouseMove;
            pxImage.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);

            tbPenWidth.Value = Convert.ToInt32(myPen.Width);
            tbPenWidth.ValueChanged += (s, e) => myPen.Width = tbPenWidth.Value;

            buColorMain.Click += ChouseColor;
            buColorSecond.Click += ChouseColor;
            tSMSaveAs.Click += BuImageSave_Click;
            tSMLoadfromFile.Click+= BuImageLoad_Click;
            tSMClear.Click += delegate
             {
                 g.Clear(Color.White);
                 pxImage.Invalidate();
             };
            tSMStars.Click+= BuAddStars_Click;
         
      
        }

    
     

        private void ChouseColor(object sender, EventArgs e)
        {
            ColorDialog CD = new ColorDialog();
            if (CD.ShowDialog() == DialogResult.Cancel)
                return;
            Button button = (Button)sender;
            button.BackColor = CD.Color;
        }

        private void BuAddStars_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            for (int i = 0; i < 100; i++)
            {
                g.FillEllipse(new SolidBrush(Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256))),
                    rnd.Next(b.Width),
                    rnd.Next(b.Height),
                    rnd.Next(1, 10),
                    rnd.Next(1, 10)
                    );
            }
            pxImage.Invalidate();
        }

        private void BuImageLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            if (dialog.ShowDialog()==DialogResult.OK)
            {
                g.Clear(SystemColors.Control);
                g.DrawImage(Bitmap.FromFile(dialog.FileName), 0, 0);
                pxImage.Invalidate();
            }
        }

        private void BuImageSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "JPG Files(*.JPG)|*.JPG;";
            if (dialog.ShowDialog()==DialogResult.OK)
            {
                b.Save(dialog.FileName);
            }
        }

        private void PxImage_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button==MouseButtons.Left)
            {
                myPen.Color = buColorMain.BackColor;
                g.DrawLine(myPen, startPoint, e.Location);
                startPoint = e.Location;
                pxImage.Invalidate();
            }
            if (e.Button == MouseButtons.Right)
            {
                myPen.Color = buColorSecond.BackColor;
                g.DrawLine(myPen, startPoint, e.Location);
                startPoint = e.Location;
                pxImage.Invalidate();
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
