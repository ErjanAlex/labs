﻿namespace LabPaint
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelPen = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbPenWidth = new System.Windows.Forms.TrackBar();
            this.buColorSecond = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.buColorMain = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.tSMFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tSMSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.tSMLoadfromFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tSMConva = new System.Windows.Forms.ToolStripMenuItem();
            this.tSMClear = new System.Windows.Forms.ToolStripMenuItem();
            this.tSMStars = new System.Windows.Forms.ToolStripMenuItem();
            this.panelPen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbPenWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelPen
            // 
            this.panelPen.Controls.Add(this.label5);
            this.panelPen.Controls.Add(this.label4);
            this.panelPen.Controls.Add(this.label3);
            this.panelPen.Controls.Add(this.tbPenWidth);
            this.panelPen.Controls.Add(this.buColorSecond);
            this.panelPen.Controls.Add(this.label2);
            this.panelPen.Controls.Add(this.buColorMain);
            this.panelPen.Controls.Add(this.label1);
            this.panelPen.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelPen.Location = new System.Drawing.Point(0, 24);
            this.panelPen.Name = "panelPen";
            this.panelPen.Size = new System.Drawing.Size(146, 355);
            this.panelPen.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.Location = new System.Drawing.Point(16, 284);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 19);
            this.label5.TabIndex = 8;
            this.label5.Text = "Толщина пера";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label5.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Location = new System.Drawing.Point(12, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 23);
            this.label4.TabIndex = 8;
            this.label4.Text = "Правая кнопка мыши";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.Click += new System.EventHandler(this.label3_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(12, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 23);
            this.label3.TabIndex = 8;
            this.label3.Text = "Левая кнопка мыши";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // tbPenWidth
            // 
            this.tbPenWidth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPenWidth.Location = new System.Drawing.Point(26, 306);
            this.tbPenWidth.Minimum = 1;
            this.tbPenWidth.Name = "tbPenWidth";
            this.tbPenWidth.Size = new System.Drawing.Size(102, 45);
            this.tbPenWidth.TabIndex = 1;
            this.tbPenWidth.Value = 1;
            // 
            // buColorSecond
            // 
            this.buColorSecond.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buColorSecond.BackColor = System.Drawing.Color.Yellow;
            this.buColorSecond.Location = new System.Drawing.Point(85, 170);
            this.buColorSecond.Name = "buColorSecond";
            this.buColorSecond.Size = new System.Drawing.Size(24, 23);
            this.buColorSecond.TabIndex = 7;
            this.buColorSecond.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 174);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "Побочный";
            // 
            // buColorMain
            // 
            this.buColorMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buColorMain.BackColor = System.Drawing.Color.Lime;
            this.buColorMain.Location = new System.Drawing.Point(85, 35);
            this.buColorMain.Name = "buColorMain";
            this.buColorMain.Size = new System.Drawing.Size(24, 23);
            this.buColorMain.TabIndex = 7;
            this.buColorMain.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "Основной";
            // 
            // pxImage
            // 
            this.pxImage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pxImage.BackColor = System.Drawing.Color.White;
            this.pxImage.Location = new System.Drawing.Point(152, 24);
            this.pxImage.Name = "pxImage";
            this.pxImage.Size = new System.Drawing.Size(775, 343);
            this.pxImage.TabIndex = 1;
            this.pxImage.TabStop = false;
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSMFile,
            this.tSMConva});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(939, 24);
            this.menu.TabIndex = 2;
            this.menu.Text = "menuStrip1";
            // 
            // tSMFile
            // 
            this.tSMFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSMSaveAs,
            this.tSMLoadfromFile});
            this.tSMFile.Name = "tSMFile";
            this.tSMFile.Size = new System.Drawing.Size(48, 20);
            this.tSMFile.Text = "Файл";
            // 
            // tSMSaveAs
            // 
            this.tSMSaveAs.Name = "tSMSaveAs";
            this.tSMSaveAs.Size = new System.Drawing.Size(154, 22);
            this.tSMSaveAs.Text = "Сохранить как";
            // 
            // tSMLoadfromFile
            // 
            this.tSMLoadfromFile.Name = "tSMLoadfromFile";
            this.tSMLoadfromFile.Size = new System.Drawing.Size(154, 22);
            this.tSMLoadfromFile.Text = "Загрузить";
            // 
            // tSMConva
            // 
            this.tSMConva.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSMClear,
            this.tSMStars});
            this.tSMConva.Name = "tSMConva";
            this.tSMConva.Size = new System.Drawing.Size(51, 20);
            this.tSMConva.Text = "Холст";
            // 
            // tSMClear
            // 
            this.tSMClear.Name = "tSMClear";
            this.tSMClear.Size = new System.Drawing.Size(206, 22);
            this.tSMClear.Text = "Очистить";
            // 
            // tSMStars
            // 
            this.tSMStars.Name = "tSMStars";
            this.tSMStars.Size = new System.Drawing.Size(206, 22);
            this.tSMStars.Text = "Заполнить звездочками";
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 379);
            this.Controls.Add(this.panelPen);
            this.Controls.Add(this.pxImage);
            this.Controls.Add(this.menu);
            this.MainMenuStrip = this.menu;
            this.MinimumSize = new System.Drawing.Size(354, 412);
            this.Name = "Fm";
            this.Text = "LabPaint";
            this.panelPen.ResumeLayout(false);
            this.panelPen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbPenWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelPen;
        private System.Windows.Forms.TrackBar tbPenWidth;
        private System.Windows.Forms.PictureBox pxImage;
        private System.Windows.Forms.Button buColorSecond;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buColorMain;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem tSMSaveAs;
        private System.Windows.Forms.ToolStripMenuItem tSMFile;
        private System.Windows.Forms.ToolStripMenuItem tSMLoadfromFile;
        private System.Windows.Forms.ToolStripMenuItem tSMConva;
        private System.Windows.Forms.ToolStripMenuItem tSMClear;
        private System.Windows.Forms.ToolStripMenuItem tSMStars;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}

