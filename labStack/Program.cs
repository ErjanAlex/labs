﻿using System;
using System.Collections;

namespace labStack
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack x = new Stack();
            x.Push("Hello");
            x.Push("Word");
            x.Push(123);

            Console.WriteLine(x.Peek()); // Посмотреть
            Console.WriteLine("---");

            Console.WriteLine(x.Pop());
            Console.WriteLine(x.Pop());
            Console.WriteLine(x.Pop());

            try
            {
                Console.WriteLine(x.Pop());
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Что-то пошло не так : {ex.Message}");
            }
        }
    }
}
