﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labTimer
{
    public partial class Fm : Form
    {
        private const int SEC_MAX = 10; 

        Timer tmUp = new Timer();
        Timer tmDown = new Timer();

        private DateTime startTimeUp;
        private DateTime startTimeDown;

        private DateTime endTimerDown;

        private DateTime pauseTimerUp;
        private DateTime pauseTimerDown;
        

        public Fm()
        {
            InitializeComponent();

            Timer timer = new Timer();
            timer.Interval = 1000; // 1sec
            timer.Tick += (s, e) => this.Text += "*";
            timer.Start();

            tmUp.Interval = 100;
            tmUp.Tick += TmUp_Tick;
            buUp.Click += BuUp_Click;
            buUpPause.Click += BuUpPause_Click;
            buDownPause.Click += BuDownPause_Click;
            pbUp.Maximum = SEC_MAX;
            pbUpMs.Maximum = SEC_MAX * 1000;

            pbDown.Maximum = SEC_MAX;
            pbDown.Value = pbDown.Maximum;
            pbDownMs.Maximum = SEC_MAX * 1000;
            pbDownMs.Value = pbDownMs.Maximum;



            tmDown.Interval = 100;
            tmDown.Tick += TmDown_Tick;

            buDown.Click += BuDown_Click;


            // ХВ
            //
            //добавить паузы для tmDown
            //добавить прогресс барЫ для tmDown
            //добавить поле с отображенияем % выполнения для двух таймеров
            //


        }

        private void BuDownPause_Click(object sender, EventArgs e)
        {
            if (tmDown.Enabled)
            {
                pauseTimerDown = DateTime.Now;
                tmDown.Stop();
            }
            else
            {
                endTimerDown += DateTime.Now - pauseTimerDown;
                tmDown.Start();
            }
        }

        private void BuUpPause_Click(object sender, EventArgs e)
        {
            if (tmUp.Enabled)
            {
                pauseTimerUp = DateTime.Now;
                tmUp.Stop();
            }
            else
            {
                startTimeUp += DateTime.Now - pauseTimerUp;
                tmUp.Start();
            }

        }

        private void BuDown_Click(object sender, EventArgs e)
        {
            tmDown.Enabled = !tmDown.Enabled;
            endTimerDown = DateTime.Now.AddSeconds(SEC_MAX);




        }

        private void TmDown_Tick(object sender, EventArgs e)
        {
            var x = endTimerDown - DateTime.Now;
            if (x.Ticks < 0)
            {
                tmDown.Stop();
                x = TimeSpan.Zero;
            }
            buDown.Text = x.ToString(@"mm\:ss\.ffff");

            pbDown.Value = (int)x.TotalSeconds;
            pbDownMs.Value = (int)x.TotalMilliseconds;

            double proc = x.TotalMilliseconds / (SEC_MAX * 1000);
            labelProcDownTimer.Text = proc.ToString(@"0.00 %");

        }

        private void TmUp_Tick(object sender, EventArgs e)
        {
            //(1)
            var x = DateTime.Now - startTimeUp;
            if (x.TotalSeconds > SEC_MAX)        
            {
                tmUp.Stop();
                x = TimeSpan.FromSeconds(SEC_MAX);
            }
            buUp.Text = x.ToString(@"mm\:ss\.ffff");

            pbUp.Value = (int)x.TotalSeconds;
            pbUpMs.Value = (int)x.TotalMilliseconds;


            double proc = x.TotalMilliseconds / (SEC_MAX * 1000);
            labelProcUpTimer.Text = proc.ToString(@"0.00 %");
            //(2)
            //buUp.Text = (DateTime.Now - startTimeUp).ToString();
        }

        private void BuUp_Click(object sender, EventArgs e)
        {
            tmUp.Enabled = !tmUp.Enabled;
            startTimeUp = DateTime.Now;
        }

        private void buUpPause_Click_1(object sender, EventArgs e)
        {

        }
    }
}
