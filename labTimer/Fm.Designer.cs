﻿namespace labTimer
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buUp = new System.Windows.Forms.Button();
            this.buDown = new System.Windows.Forms.Button();
            this.buUpPause = new System.Windows.Forms.Button();
            this.buDownPause = new System.Windows.Forms.Button();
            this.pbUp = new System.Windows.Forms.ProgressBar();
            this.pbUpMs = new System.Windows.Forms.ProgressBar();
            this.pbDown = new System.Windows.Forms.ProgressBar();
            this.pbDownMs = new System.Windows.Forms.ProgressBar();
            this.labelProcUpTimer = new System.Windows.Forms.Label();
            this.labelProcDownTimer1 = new System.Windows.Forms.Label();
            this.labelProcDownTimer = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buUp
            // 
            this.buUp.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buUp.Location = new System.Drawing.Point(12, 12);
            this.buUp.Name = "buUp";
            this.buUp.Size = new System.Drawing.Size(266, 107);
            this.buUp.TabIndex = 0;
            this.buUp.Text = "Start GO";
            this.buUp.UseVisualStyleBackColor = true;
            // 
            // buDown
            // 
            this.buDown.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buDown.Location = new System.Drawing.Point(12, 126);
            this.buDown.Name = "buDown";
            this.buDown.Size = new System.Drawing.Size(265, 98);
            this.buDown.TabIndex = 1;
            this.buDown.Text = "Start BACK";
            this.buDown.UseVisualStyleBackColor = true;
            // 
            // buUpPause
            // 
            this.buUpPause.Font = new System.Drawing.Font("Segoe UI", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buUpPause.Location = new System.Drawing.Point(293, 12);
            this.buUpPause.Name = "buUpPause";
            this.buUpPause.Size = new System.Drawing.Size(130, 107);
            this.buUpPause.TabIndex = 2;
            this.buUpPause.Text = "⏸";
            this.buUpPause.UseVisualStyleBackColor = true;
            this.buUpPause.Click += new System.EventHandler(this.buUpPause_Click_1);
            // 
            // buDownPause
            // 
            this.buDownPause.Font = new System.Drawing.Font("Segoe UI", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buDownPause.Location = new System.Drawing.Point(293, 126);
            this.buDownPause.Name = "buDownPause";
            this.buDownPause.Size = new System.Drawing.Size(130, 98);
            this.buDownPause.TabIndex = 3;
            this.buDownPause.Text = "⏸";
            this.buDownPause.UseVisualStyleBackColor = true;
            // 
            // pbUp
            // 
            this.pbUp.Location = new System.Drawing.Point(429, 38);
            this.pbUp.Name = "pbUp";
            this.pbUp.Size = new System.Drawing.Size(336, 23);
            this.pbUp.TabIndex = 4;
            // 
            // pbUpMs
            // 
            this.pbUpMs.Location = new System.Drawing.Point(429, 78);
            this.pbUpMs.Name = "pbUpMs";
            this.pbUpMs.Size = new System.Drawing.Size(336, 23);
            this.pbUpMs.TabIndex = 4;
            // 
            // pbDown
            // 
            this.pbDown.Location = new System.Drawing.Point(429, 162);
            this.pbDown.Name = "pbDown";
            this.pbDown.Size = new System.Drawing.Size(336, 23);
            this.pbDown.TabIndex = 4;
            // 
            // pbDownMs
            // 
            this.pbDownMs.Location = new System.Drawing.Point(429, 202);
            this.pbDownMs.Name = "pbDownMs";
            this.pbDownMs.Size = new System.Drawing.Size(336, 23);
            this.pbDownMs.TabIndex = 4;
            // 
            // labelProcUpTimer
            // 
            this.labelProcUpTimer.AutoSize = true;
            this.labelProcUpTimer.Location = new System.Drawing.Point(578, 20);
            this.labelProcUpTimer.Name = "labelProcUpTimer";
            this.labelProcUpTimer.Size = new System.Drawing.Size(33, 15);
            this.labelProcUpTimer.TabIndex = 5;
            this.labelProcUpTimer.Text = "TIME";
            // 
            // labelProcDownTimer1
            // 
            this.labelProcDownTimer1.AutoSize = true;
            this.labelProcDownTimer1.Location = new System.Drawing.Point(95, -57);
            this.labelProcDownTimer1.Name = "labelProcDownTimer1";
            this.labelProcDownTimer1.Size = new System.Drawing.Size(0, 15);
            this.labelProcDownTimer1.TabIndex = 6;
            // 
            // labelProcDownTimer
            // 
            this.labelProcDownTimer.AutoSize = true;
            this.labelProcDownTimer.Location = new System.Drawing.Point(578, 144);
            this.labelProcDownTimer.Name = "labelProcDownTimer";
            this.labelProcDownTimer.Size = new System.Drawing.Size(33, 15);
            this.labelProcDownTimer.TabIndex = 7;
            this.labelProcDownTimer.Text = "TIME";
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 253);
            this.Controls.Add(this.labelProcDownTimer);
            this.Controls.Add(this.labelProcDownTimer1);
            this.Controls.Add(this.labelProcUpTimer);
            this.Controls.Add(this.pbDownMs);
            this.Controls.Add(this.pbDown);
            this.Controls.Add(this.pbUpMs);
            this.Controls.Add(this.pbUp);
            this.Controls.Add(this.buDownPause);
            this.Controls.Add(this.buUpPause);
            this.Controls.Add(this.buDown);
            this.Controls.Add(this.buUp);
            this.Name = "Fm";
            this.Text = "labTimer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buUp;
        private System.Windows.Forms.Button buDown;
        private System.Windows.Forms.Button buUpPause;
        private System.Windows.Forms.Button buDownPause;
        private System.Windows.Forms.ProgressBar pbUp;
        private System.Windows.Forms.ProgressBar pbUpMs;
        private System.Windows.Forms.ProgressBar pbDown;
        private System.Windows.Forms.ProgressBar pbDownMs;
        private System.Windows.Forms.Label labelProcUpTimer;
        private System.Windows.Forms.Label labelProcDownTimer1;
        private System.Windows.Forms.Label labelProcDownTimer;
    }
}

