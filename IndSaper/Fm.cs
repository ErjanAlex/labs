﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Fm : Form
    {
        private const int
          MR = 10, 
          MC = 10, 
          NM = 11, 
          W = 40, 
          H = 40; 
        private int[,] Pole = new int[MR + 2, MC + 2];

        private int nMin;  
        private int nFlag; 
        private int status;
        private System.Drawing.Graphics g;

        public Fm()
        {
            InitializeComponent();

            
            for(int row = 0; row <= MR+1; row++)
            {
                Pole[row,0] = -3;
                Pole[row,MC+1] = -3;
            }
                
            for(int col = 0; col <= MC+1; col++)
            {
                Pole[0,col] = -3;
                Pole[MR+1,col] = -3;            
            }

           
            this.ClientSize = new Size(W*MC + 1, H*MR +  menuStrip1.Height +  1);

            newGame(); 

            g = panel1.CreateGraphics();
        }
private void newGame() {
    int row, col;   
    int n = 0;       
    int k;           

    for(row = 1; row <= MR; row++)
        for(col = 1; col <= MC; col++)
            Pole[row,col] = 0;

    Random rnd = new Random();

    do
    {
        row = rnd.Next(MR) + 1;
        col = rnd.Next(MC) + 1;

        if (Pole[row,col] != 9)
        {
            Pole[row,col] = 9;
            n++;
        }
    }
    while (n != NM);

    for(row = 1; row <= MR; row++)
        for(col = 1; col <= MC; col++)
            if (Pole[row,col] != 9)
            {
                k = 0;

                if (Pole[row-1,col-1] == 9) k++;
                if (Pole[row-1,col]   == 9) k++;
                if (Pole[row-1,col+1] == 9) k++;
                if (Pole[row,col-1]   == 9) k++;
                if (Pole[row,col+1]   == 9) k++;
                if (Pole[row+1,col-1] == 9) k++;
                if (Pole[row+1,col]   == 9) k++;
                if (Pole[row+1,col+1] == 9) k++;

                Pole[row,col] = k;
            }

    status = 0;      
    nMin   = 0;      
    nFlag  = 0;     
}
private void showPole(Graphics g, int status) {
    for(int row = 1; row <= MR; row++)
        for(int col = 1; col <= MC; col++)
            this.kletka(g, row, col, status);
}
private void kletka(Graphics g,
    int row, int col, int status) {
    
    int x,y;
    
    x = (col - 1) * W + 1;
    y = (row-1)* H + 1;

  
    if (Pole[row,col] < 100)
        g.FillRectangle(SystemBrushes.ControlLight,
            x-1, y-1, W, H);


    if (Pole[row,col] >= 100) {

  
        if (Pole[row,col] != 109)
            g.FillRectangle(Brushes.White,
                x-1, y-1, W, H);
        else

            g.FillRectangle(Brushes.Red,
                x-1, y-1, W, H);


        if ((Pole[row,col] >= 101) && (Pole[row,col] <= 108))
            g.DrawString((Pole[row,col]-100).ToString(),
                new Font("Tahoma", 10,
                    System.Drawing.FontStyle.Regular),
                Brushes.Blue, x+3, y+2);
    }

    if (Pole[row,col] >= 200)
        this.flag(g, x, y);


    g.DrawRectangle(Pens.Black,
        x-1, y-1, W, H);


    if ((status == 2) && ((Pole[row,col] % 10) == 9))
        this.mina(g, x, y);
}

private void open(int row, int col)
{

    int x = (col-1)* W + 1,
        y = (row-1)* H + 1;
    
    if (Pole[row,col] == 0)
    {
        Pole[row,col] = 100;

        this.kletka(g, row, col, status);

        this.open(row, col-1);
        this.open(row-1, col);
        this.open(row, col+1);
        this.open(row+1, col);

        this.open(row-1,col-1);
        this.open(row-1,col+1);
        this.open(row+1,col-1);
        this.open(row+1,col+1);
    }
    else
        if ((Pole[row,col] < 100) &&
             (Pole[row,col] != -3))
        {
            Pole[row,col] += 100;

            this.kletka(g, row, col, status);
        }
}
private void mina(Graphics g, int x, int y)
{

    g.FillRectangle(Brushes.Green,
        x+16, y+26, 8, 4);
    g.FillRectangle(Brushes.Green,
        x+8, y+30, 24, 4);
    g.DrawPie(Pens.Black,
        x+6, y+28, 28, 16, 0, -180);
    g.FillPie(Brushes.Green,
        x+6, y+28, 28, 16, 0, -180);
}
private void flag(Graphics g, int x, int y)
{
    Point[] p = new Point[3];
    Point[] m = new Point[5];            

    p[0].X = x+4;   p[0].Y = y+4;
    p[1].X = x+30;  p[1].Y = y+12;
    p[2].X = x+4;   p[2].Y = y+20;
    g.FillPolygon(Brushes.Red, p);

    g.DrawLine(Pens.Black,
        x+4, y+4, x+4, y+35);
}

private void panel1_MouseClick(object sender, MouseEventArgs e)
{
    if (status == 2) return;
    if (status == 0) status = 1;
          
    int row = (int)(e.Y/H) + 1,
        col = (int)(e.X/W) + 1;

    int x = (col-1)* W + 1,
        y = (row-1)* H + 1;

    if (e.Button == MouseButtons.Left)
    {                
                   
        if (Pole[row,col] == 9)
        {                
            Pole[row,col] += 100;
            status = 2;
            this.panel1.Invalidate();
        }
        else
            if (Pole[row,col] < 9)
                this.open(row,col);
    }

    if (e.Button == MouseButtons.Right) {

        if (Pole[row,col] <= 9) {
            nFlag += 1;

            if (Pole[row,col] == 9)
                nMin += 1;

            Pole[row,col] += 200;

            if ((nMin == NM) && (nFlag == NM)) {
                status = 2;

                this.Invalidate();
            }
            else
                this.kletka(g, row, col, status);
        }
        else

            if (Pole[row,col] >= 200)
            {                                 
                nFlag -= 1;
                Pole[row,col] -= 200;             
                this.kletka(g, row, col, status);
            }                
    }
}

private void новаяИграToolStripMenuItem_Click(object sender, EventArgs e)
{
    newGame();
    showPole(g, status);
}
private void panel1_Paint(object sender, PaintEventArgs e)
{
    showPole(g, status);
}

    }
}
