﻿namespace labFileExplorer
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Fm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.buBack = new System.Windows.Forms.ToolStripButton();
            this.buForward = new System.Windows.Forms.ToolStripButton();
            this.buUP = new System.Windows.Forms.ToolStripButton();
            this.edDir = new System.Windows.Forms.ToolStripTextBox();
            this.buDirSelect = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.miViewLargeIcon = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewSmallIcon = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewList = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewDetails = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewTile = new System.Windows.Forms.ToolStripMenuItem();
            this.buCopyFileNamesToClipBoard = new System.Windows.Forms.ToolStripButton();
            this.ilLargeIcon = new System.Windows.Forms.ImageList(this.components);
            this.ilSmallIcon = new System.Windows.Forms.ImageList(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miPopupOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miPopupDel = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.miPopupProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.laStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lv = new System.Windows.Forms.ListView();
            this.tv = new System.Windows.Forms.TreeView();
            this.toolStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buBack,
            this.buForward,
            this.buUP,
            this.edDir,
            this.buDirSelect,
            this.toolStripDropDownButton1,
            this.buCopyFileNamesToClipBoard});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // buBack
            // 
            this.buBack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buBack.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buBack.Name = "buBack";
            this.buBack.Size = new System.Drawing.Size(23, 22);
            this.buBack.Text = "☚";
            // 
            // buForward
            // 
            this.buForward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buForward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buForward.Name = "buForward";
            this.buForward.Size = new System.Drawing.Size(23, 22);
            this.buForward.Text = "☛";
            // 
            // buUP
            // 
            this.buUP.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buUP.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buUP.Name = "buUP";
            this.buUP.Size = new System.Drawing.Size(23, 22);
            this.buUP.Text = "🠕";
            // 
            // edDir
            // 
            this.edDir.Name = "edDir";
            this.edDir.Size = new System.Drawing.Size(500, 25);
            // 
            // buDirSelect
            // 
            this.buDirSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buDirSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buDirSelect.Name = "buDirSelect";
            this.buDirSelect.Size = new System.Drawing.Size(23, 22);
            this.buDirSelect.Text = "...";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miViewLargeIcon,
            this.miViewSmallIcon,
            this.miViewList,
            this.miViewDetails,
            this.miViewTile});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(40, 22);
            this.toolStripDropDownButton1.Text = "Вид";
            // 
            // miViewLargeIcon
            // 
            this.miViewLargeIcon.Name = "miViewLargeIcon";
            this.miViewLargeIcon.Size = new System.Drawing.Size(180, 22);
            this.miViewLargeIcon.Text = "toolStripMenuItem1";
            // 
            // miViewSmallIcon
            // 
            this.miViewSmallIcon.Name = "miViewSmallIcon";
            this.miViewSmallIcon.Size = new System.Drawing.Size(180, 22);
            this.miViewSmallIcon.Text = "toolStripMenuItem2";
            // 
            // miViewList
            // 
            this.miViewList.Name = "miViewList";
            this.miViewList.Size = new System.Drawing.Size(180, 22);
            this.miViewList.Text = "toolStripMenuItem3";
            // 
            // miViewDetails
            // 
            this.miViewDetails.Name = "miViewDetails";
            this.miViewDetails.Size = new System.Drawing.Size(180, 22);
            this.miViewDetails.Text = "toolStripMenuItem4";
            // 
            // miViewTile
            // 
            this.miViewTile.Name = "miViewTile";
            this.miViewTile.Size = new System.Drawing.Size(180, 22);
            this.miViewTile.Text = "toolStripMenuItem5";
            // 
            // buCopyFileNamesToClipBoard
            // 
            this.buCopyFileNamesToClipBoard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buCopyFileNamesToClipBoard.Image = ((System.Drawing.Image)(resources.GetObject("buCopyFileNamesToClipBoard.Image")));
            this.buCopyFileNamesToClipBoard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buCopyFileNamesToClipBoard.Name = "buCopyFileNamesToClipBoard";
            this.buCopyFileNamesToClipBoard.Size = new System.Drawing.Size(70, 22);
            this.buCopyFileNamesToClipBoard.Text = "Коп.имена";
            // 
            // ilLargeIcon
            // 
            this.ilLargeIcon.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.ilLargeIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilLargeIcon.ImageStream")));
            this.ilLargeIcon.TransparentColor = System.Drawing.Color.Transparent;
            this.ilLargeIcon.Images.SetKeyName(0, "folder.jpg");
            this.ilLargeIcon.Images.SetKeyName(1, "icon file.png");
            // 
            // ilSmallIcon
            // 
            this.ilSmallIcon.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.ilSmallIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilSmallIcon.ImageStream")));
            this.ilSmallIcon.TransparentColor = System.Drawing.Color.Transparent;
            this.ilSmallIcon.Images.SetKeyName(0, "folder.jpg");
            this.ilSmallIcon.Images.SetKeyName(1, "icon file.png");
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miPopupOpen,
            this.toolStripSeparator1,
            this.miPopupDel,
            this.toolStripSeparator2,
            this.miPopupProperties});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(128, 82);
            // 
            // miPopupOpen
            // 
            this.miPopupOpen.Name = "miPopupOpen";
            this.miPopupOpen.Size = new System.Drawing.Size(127, 22);
            this.miPopupOpen.Text = "Open";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(124, 6);
            // 
            // miPopupDel
            // 
            this.miPopupDel.Name = "miPopupDel";
            this.miPopupDel.Size = new System.Drawing.Size(127, 22);
            this.miPopupDel.Text = "Delete";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(124, 6);
            // 
            // miPopupProperties
            // 
            this.miPopupProperties.Name = "miPopupProperties";
            this.miPopupProperties.Size = new System.Drawing.Size(127, 22);
            this.miPopupProperties.Text = "Properties";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.laStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(800, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // laStatus
            // 
            this.laStatus.Name = "laStatus";
            this.laStatus.Size = new System.Drawing.Size(12, 17);
            this.laStatus.Text = "-";
            // 
            // lv
            // 
            this.lv.Dock = System.Windows.Forms.DockStyle.Right;
            this.lv.HideSelection = false;
            this.lv.LargeImageList = this.ilLargeIcon;
            this.lv.Location = new System.Drawing.Point(210, 25);
            this.lv.Name = "lv";
            this.lv.Size = new System.Drawing.Size(590, 403);
            this.lv.SmallImageList = this.ilSmallIcon;
            this.lv.TabIndex = 1;
            this.lv.UseCompatibleStateImageBehavior = false;
            // 
            // tv
            // 
            this.tv.Dock = System.Windows.Forms.DockStyle.Left;
            this.tv.Location = new System.Drawing.Point(0, 25);
            this.tv.Name = "tv";
            this.tv.Size = new System.Drawing.Size(204, 403);
            this.tv.TabIndex = 3;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tv);
            this.Controls.Add(this.lv);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "Fm";
            this.Text = "Form1";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton buBack;
        private System.Windows.Forms.ToolStripButton buForward;
        private System.Windows.Forms.ToolStripButton buUP;
        private System.Windows.Forms.ToolStripTextBox edDir;
        private System.Windows.Forms.ToolStripButton buDirSelect;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem miViewLargeIcon;
        private System.Windows.Forms.ToolStripMenuItem miViewSmallIcon;
        private System.Windows.Forms.ToolStripMenuItem miViewList;
        private System.Windows.Forms.ToolStripMenuItem miViewDetails;
        private System.Windows.Forms.ToolStripMenuItem miViewTile;
        private System.Windows.Forms.ImageList ilLargeIcon;
        private System.Windows.Forms.ImageList ilSmallIcon;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miPopupOpen;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem miPopupDel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem miPopupProperties;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ListView lv;
        private System.Windows.Forms.ToolStripStatusLabel laStatus;
        private System.Windows.Forms.ToolStripButton buCopyFileNamesToClipBoard;
        private System.Windows.Forms.TreeView tv;
    }
}

