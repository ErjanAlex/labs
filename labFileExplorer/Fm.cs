﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFileExplorer
{
    public partial class Fm : Form { 
        public string CurDir { get; private set; }
        public string SelTtem { get; private set; }

        Stack<string> PathHist = new Stack<string>();

        public Fm()
        {
            InitializeComponent();

            //CurDir = "C:/";
            CurDir = Directory.GetCurrentDirectory();

            
            //buForward.Click +=
            buUP.Click += (s, e) => LoadDir(Directory.GetParent(CurDir).ToString());
            edDir.KeyDown += EdDir_KeyDown;
            buDirSelect.Click += BuDirSelect_Click;
            buCopyFileNamesToClipBoard.Click += BuCopyFileNamesToClipBoard_Click;

            miViewLargeIcon.Click += (s, e) => lv.View = View.LargeIcon;
            miViewSmallIcon.Click += (s, e) => lv.View = View.SmallIcon;
            miViewList.Click += (s, e) => lv.View = View.List;
            miViewDetails.Click += (s, e) => lv.View = View.Details;
            miViewTile.Click += (s, e) => lv.View = View.Tile;

            tv.Click += Tv_Click;
            tv.DoubleClick += Tv_DoubleClick;



            lv.ItemSelectionChanged += (s, e) => SelTtem = Path.Combine(CurDir, e.Item.Text);
            lv.DoubleClick += (s, e) => LoadDir(SelTtem);
            lv.ItemSelectionChanged += Lv_ItemSelectionChanged;

            lv.ContextMenuStrip = contextMenuStrip1;
            miPopupOpen.Click += (s, e) => LoadDir(SelTtem);
            miPopupDel.Click += MiPopupDel_Click;
            miPopupProperties.Click += MiPopupProperties_Click;

            buUP.Click += BuUP_Click;

            

            //(1)
            //var c1 = new ColumnHeader();
            //c1.Text = "Name";
            //c1.Width = 400;
            //lv.Columns.Add(c1);
            //(2)
            //lv.Columns.Add(new ColumnHeader() { Text = "Name", Width = 400 });
            //(3)
            lv.Columns.Add("Name", 400);
            lv.Columns.Add("Date change", 150);
            lv.Columns.Add("Type", 150);
            lv.Columns.Add("Size", 150);


            LoadDir(CurDir);

            this.Text += " : Drivers=" + string.Join(" ",Directory.GetLogicalDrives());


            foreach (var item in Directory.GetLogicalDrives())

            {
                tv.Nodes.Add(item);
            }


        }

        private void BuUP_Click(object sender, EventArgs e)
        {

            if (Path.GetPathRoot(CurDir) == CurDir)
            {

            }
            else
            {
                LoadDir(Directory.GetParent(CurDir).ToString());
                PathHist.Push(CurDir);
            }





            }

        private void Tv_DoubleClick(object sender, EventArgs e)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(tv.SelectedNode.FullPath);
            tv.BeginUpdate();
            tv.SelectedNode.Nodes.Clear();
            foreach (var item in directoryInfo.GetDirectories())
            {
                tv.SelectedNode.Nodes.Add(item.Name);
            }
            tv.SelectedNode.Expand();
            tv.EndUpdate();
        }

        private void Tv_Click(object sender, EventArgs e)
        {
            LoadDir(tv.SelectedNode.FullPath);
        }

        private void BuCopyFileNamesToClipBoard_Click(object sender, EventArgs e)
        {
            string st = "";
            //(1) из ListView
            foreach (var item in lv.Items)
            {
                st += item.ToString() + Environment.NewLine;
            }
            //(2) из DirectoryInfo
            //TODO
            Clipboard.SetText(st);
        }

        private void Lv_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            SelTtem = Path.Combine(CurDir, e.Item.Text);
            laStatus.Text = $"Элементов: {lv.Items.Count}, Выбрано: {lv.SelectedItems.Count}, (размер)";
        }

        private void MiPopupProperties_Click(object sender, EventArgs e)
        {
            if (File.Exists(SelTtem))
            {
                var x = new FmFileProperties(SelTtem);
                x.ShowDialog();
            }
            else if (Directory.Exists(SelTtem))
            {
                //HW (отдельная форма для просмотра свойств папки)
            }
        }

        private void MiPopupDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Delete?", 
                Application.ProductName,MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
            {
                MessageBox.Show($"File [{SelTtem}] - delete!");
                //File.Delete(SelItem);
            }
        }


        //HW
        // Проверка на buUp
        // Добавить механизм на кнопки buBack и buForward, например через стек
        // Добавить различные иконки к файлам Item.Extension
        // добавить кнопки с дисками (картинка + текст) на отдельной панели Directory.GetLogicalDrivers
        // По кнопке backspace перейти на уровень выше (buUp)
        // Добавить механизм открытия файла (например отдельная форма или показать путь к файлу(текст или картинка))
        // Добавить кноку "создать папку" 



        // PART 2

        // Рахмер файла отображать в формате 00 000 000 (байт, КБ, Мб, ГБ...)
        // Добавить кнопку выбора вида отображения разммера файла: автоматически, байт, КБ, МБ, ГБ...
        // По кнопке пробел считать размер папки и показать в столбце "Размер"
        // Добавить кнопку "показать/скрыть скрытые папки и файлы"
        // Добавить панель с детальной информацией о файле /папке + кнопка вкл/выкл панель
        // Добавить механизм избранной папки. Добавить отдельную панель, кнопка добавление в избранное, переход, удаление из избранного +кнопка вкл/выкл
        //

        // PART 3
        // Доделать контестное меню (открыть, закрыть и тп.)
        // Доделать форму свойства файла 
        // Создать форму сфойства папки 
        // По кнопке DEL удалять файл\папку с предупреждением 
        // Доделать нижний статус бар
        // По кнопке сохранить имена файлов в буфер обмена
        // Добавить копирование, вставка, файлов через ClipBoard (в контекстное меню)
        // Добавить кнопку на отображение/скрытие treeView слева
        // Доработать отображение данных в treeView(скрытые папки, иконки ...)
        // Добавить различные иконки (hdd, скрытые папки, пустые папки, папка с файлами ...)
        // Загрузка подпапок в treeview (для появления кнопочки плюсик рядом с папкой)


        //БОБИН ИЛЬЯ

        private void BuDirSelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                LoadDir(dialog.SelectedPath);
            }
        }

        private void EdDir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LoadDir(edDir.Text);
            }
        }

       

        private void LoadDir(string newDir)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(newDir);

            lv.BeginUpdate();
            lv.Items.Clear();

            foreach (var item in directoryInfo.GetDirectories())
            {
                //(1)
                //lv.Items.Add(item.Name, 0);
                //(2)
                lv.Items.Add(new ListViewItem(
                    new string[] {item.Name, item.LastWriteTime.ToString(), "File", "" },0));


            }

            foreach (var item in directoryInfo.GetFiles())
            {
                //(1)
                //lv.Items.Add(item.Name, 1);
                //(2)
                lv.Items.Add(new ListViewItem(
                    new string[] { item.Name, item.LastWriteTime.ToString(), "File", $"{item.Length} byte" }, 1));
            }

            lv.EndUpdate();
            CurDir = newDir;
            edDir.Text = newDir;
        }
    }
}
