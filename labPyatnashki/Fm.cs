﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPyatnashki
{
    public partial class Fm : Form
    {
        private GameLogic gameLogic;

        public Fm()
        {
            InitializeComponent();
            gameLogic = new GameLogic();

            gameLogic.resetGame(this);

            gameLogic.Change += Game_Change;


        }

        private void Game_Change(object sender, EventArgs e)
        {
            Text = $"Пятнашки {gameLogic.lvl} x {gameLogic.lvl}. Количество ходов: {gameLogic.countMove}";
        }
    }
   

    }
