﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace labPyatnashki
{
    class GameLogic
    {
        private Button[,] bu;
        private int lvl1;
        public int lvl=3; // МЕНЯТЬ УРОВЕНЬ
        public int countMove = 0;
        private int newWay;
        private int thisIsTheWay;
        private int oldWay;
        public event EventHandler Change;


        private void ResizeButtons(Fm fm)
        {
            int xCellWidth = fm.ClientSize.Width / lvl;
            int xCellHeight = fm.ClientSize.Height / lvl;

           
            for (int i = 0; i < lvl; i++)
            {
                for (int j = 0; j <lvl; j++)
                {
                    bu[i, j].Width = xCellWidth;
                    bu[i, j].Height = xCellHeight;
                    bu[i, j].Location = new Point(j * xCellWidth, i * xCellHeight);

                }
            }
        }

        internal void resetGame(Fm fm)
        {
            CreateButtons(fm);

            //lvl = 3;

            countMove = 0;

        }

        private void CreateButtons(Fm fm)
        {
            fm.Controls.Clear();
            bu = new Button[lvl, lvl];
            lvl1 = 0;
            for (int i = 0; i < lvl; i++)
            {
                
                for (int j = 0; j < lvl; j++)
                {
                    bu[i, j] = new Button();
                    bu[i, j].Font = new Font("Microsoft Sans Serif", 16);
                    bu[i, j].MouseEnter += Fm_MouseEnter;
                    bu[i, j].MouseClick += GameLogic_MouseClick;
                    bu[i, j].MouseLeave += Fm_MouseLeave;
                    
                    lvl1 = lvl1 + 1;
                    if (lvl1 == lvl*lvl)
                    {
                        bu[i, j].Text = $"";
                        oldWay = lvl*lvl;
                    } 
                    else
                    bu[i, j].Text = $"{lvl1}";
                    bu[i, j].Name = $"{lvl1}";
                    

                    fm.Controls.Add(bu[i, j]);
                }
            }
            
            ResizeButtons(fm);
            action();
        }

        private void GameLogic_MouseClick(object sender, MouseEventArgs e)
        {


            if (sender is Control lb)
            {
                for (int j = 0; j < lvl; ++j)
                {
                    for (int x = 0; x < lvl; ++x)
                    {

                        if (bu[j, x].Text == "") //Если пустая кнопка 
                        {
                            if ((lb.Name == (Convert.ToInt16(bu[j, x].Name) - 1).ToString()) || // Если индекс имени кнопки не заходит за пределы перемещения 
                           (lb.Name == (Convert.ToInt16(bu[j, x].Name) + 1).ToString()) ||
                           (lb.Name == (Convert.ToInt16(bu[j, x].Name) - lvl).ToString()) ||
                           (lb.Name == (Convert.ToInt16(bu[j, x].Name) + lvl).ToString()))

                            {
                                int abc = 0;
                                for (int a = 0; a < lvl; ++a)
                                {
                                    for (int b = 0; b < lvl; ++b)
                                    {
                                        if (lb.Name == bu[a, b].Name)
                                        {
                                            abc = b;
                                        }
                                    }
                                }

                                    if ((x-abc == -1) || (x - abc == 1) || (x - abc == 0))
                                    {
                                        bu[j, x].Text = lb.Text;
                                        lb.Text = "";
                                    //MessageBox.Show("OK");
                                    countMove++;
                                    Change?.Invoke(this, EventArgs.Empty);

                                    int final = 0;
                                    for (int t = 0; t < lvl; ++t)
                                    {
                                        for (int y = 0; y < lvl; ++y)
                                        {
                                            if (bu[t,y].Text == bu[t,y].Name)
                                            {
                                                final++;
                                                
                                            }
                                        }
                                    }
                                    //MessageBox.Show(final.ToString());
                                    if (final == (lvl*lvl)-1)
                                    {
                                        MessageBox.Show("Вы серьезно решили пройти это? Поздравляю.");
                                    }

                                }

                                    
                                

                                
                            }
                            
                        }

                    }
                }
            }

        }

        private void action()
        {
            int countProg = 50; // Сложность
            Random rnd = new Random();
            Button thisBU = bu[0,0];

            for (int i = 0; i < countProg; i++)
            {
                //1 left
                //2 right
                //3 up
                //4 down
                newWay = 0;

                thisIsTheWay = rnd.Next(4);
                switch (thisIsTheWay)
                {
                    case 1:
                        newWay = oldWay - 1;
                        
                    break;

                    case 2:
                        newWay = oldWay + 1;
                    break;

                    case 3:
                        newWay = oldWay - lvl;
                    break;

                    case 4:
                        newWay = oldWay + lvl;
                    break;

                }
                if ((newWay>0)&&(newWay<lvl*lvl))
                {
                    for (int j = 0; j < lvl; j++)
                    {
                        for (int x = 0; x < lvl; x++)
                        {

                            if (newWay.ToString() == bu[j, x].Name)// Поиск новой кнопки
                            {
                                thisBU = bu[j, x]; //Cохранение кнопки куда будем перемещать
                            }

                            if (bu[j, x].Text == "")// Поиск пустой кнопки 
                            {
                                //string del = bu[j, x].Text;

                                bu[j, x].Text = thisBU.Text;
                                thisBU.Text = "";
                                

                                oldWay = newWay;
                            }

                        }
                    }
                }
                


            }



        }




        private void Fm_MouseLeave(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button.BackColor = default;
        }

        private void Fm_MouseEnter(object sender, EventArgs e)
        {

            Random rnd = new Random();
            Button button = (Button)sender;
            button.BackColor = Color.FromArgb(rnd.Next(255), rnd.Next(255), rnd.Next(255));
        }
    }


}

