﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageScrollHorz
{
    public partial class Fm : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private readonly Bitmap t;
        private readonly Graphics h;
        private Bitmap imBG;
        private Bitmap imBG1;
        private Point startPoint;
        private int deltaX = 0;
        private int deltaX1 = 0;
        private int count = 0;
        Boolean tol = false;

        public Fm()

            
        {
          
        InitializeComponent();

            comboBox1.SelectedIndex = 0;


            imBG = Properties.Resources.background1;
            imBG1 = Properties.Resources.Cloud_PNG;

            this.Height = imBG.Height;

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            
            g = Graphics.FromImage(b);

            h = Graphics.FromImage(b);

            this.DoubleBuffered = true;
            this.Paint += (s, e) => { UpdateBG(); e.Graphics.DrawImage(b, 0, 0); };
            this.MouseDown += (s, e) => startPoint = e.Location;
            this.MouseMove += Fm_MouseMove;
            this.KeyDown += Fm_KeyDown;



           
           

          


            

            /*
             * Добавить несколько фонов и возможно переключатся между ними  CHECK
             * Избавиться от "i < 3", ваыбрать узкую картинку               CHECK       
             * Добавить управление с помощью клавиатуры                     CHECK
             * Добавить ускорение при нажатии стрелок                       CHECK
             * Добавить еще объекты на фон с разной скоростью               CHECK
             * 
             * 
             *  
             *  Бобин Илья
             */



        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            
            
            switch (e.KeyCode)
            {

                case Keys.Left:
                    if (tol == false) {
                        count = 0;
                    }
                    tol = true;
                    count = count + 10;
                    if (count >= 250)
                    {
                        UpdateDeltaX(45);
                    } 
                    else UpdateDeltaX(10);
                    break;

                case Keys.Right:
                    if (tol == true)
                    {
                        count = 0;
                    }
                    tol = false;
                    count = count - 10;
                    if (count <= -250)
                    {
                        UpdateDeltaX(-45);
                    } 
                    else UpdateDeltaX(-10);
                    break;
            }
            
            this.Invalidate();
        }

        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                UpdateDeltaX(e.X - startPoint.X);
                startPoint = e.Location;
                this.Invalidate();
            }
        }

        private void UpdateDeltaX(int v)
        {
            Text = $"{Application.ProductName} : deltaX = {deltaX},v = {v}, count = {count}";
            deltaX += v;
            deltaX1 += 3 * v;


            if (deltaX > 0)
            {
                deltaX -= imBG.Width;
            }
            else if (deltaX < -imBG.Width)
            {
                deltaX += imBG.Width;
            }

            if (deltaX1 > 0)
            {
                deltaX1 -= imBG1.Width ;
            }
            else if (deltaX1 < -imBG1.Width)
            {
                deltaX1 += imBG1.Width;
            }


        }

        private void UpdateBG()
        {
            //g.Clear(SystemColors.Control);
            for (int i = 0; i < imBG.Width; i++)
            {
                g.DrawImage(imBG, deltaX + i * imBG.Width, 0);
                h.DrawImage(imBG1, deltaX1 + i * imBG1.Width, 0);
            }
              
        
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                imBG = Properties.Resources.background1;
            }
            else if (comboBox1.SelectedIndex == 1)
            {
                imBG = Properties.Resources.ForestUZ;
            }
            else if (comboBox1.SelectedIndex == 2)
            {
                imBG = Properties.Resources.HorzGameTown;
            }
        }

        private void Fm_Load(object sender, EventArgs e)
        {

        }

        private void Fm_MouseEnter(object sender, EventArgs e)
        {
           
        }

        private void Fm_MouseClick(object sender, MouseEventArgs e)
        {
            comboBox1.Enabled = false;
        }

        private void Fm_DoubleClick(object sender, EventArgs e)
        {
            comboBox1.Enabled = true;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
